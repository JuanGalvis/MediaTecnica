# MediaTecnica
Software Web con diversos sistemas de información que aprendimos durante la Media Técnica de programación de Software

Para la "Feria de la Ciencia e Innovación" realizada en mi institución educativa, presente estas paginas web para mostrar lo que habiamos aprendido. Entre las paginas estan:

1- GAME: Juego de Snacke realizado por tutorial mediantre JavaScript.
2- IMC: Software que calcula el Indice de Masa Corporal ingresando la Altura y Peso del Usuario (Mediante Javascript)
3- INDICE: Pagina de inicio
4- SOMOS: Pagina con serie de Articulos sobre los programadores
5- VIDEO: Video inspirador de "Porque todos deberian aprender a programar".
